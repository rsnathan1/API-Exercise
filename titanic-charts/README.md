# What is Helm?
    Helm helps you manage Kubernetes applications — Helm Charts help you define, install, and upgrade even the most complex Kubernetes application.
    Charts are easy to create, version, share, and publish — so start using Helm and stop the copy-and-paste
    This repository contains charts which we will use to deploy postgres,titanic-api and adminer

# Install Helm 2.16.1 (Other versions maynot work with these charts)
    ## To install Helm, run the following commands:
        ```
        curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get > get_helm.sh
        chmod 700 get_helm.sh
        ./get_helm.sh
        ```
    ## Initialize Helm as shown below:

        `helm init --service-account tiller`
    ## If you have previously initialized Helm, execute the following command to upgrade it:

        `helm init --upgrade --service-account tiller`

# Directory Structure

    ├── Chart.yaml
    ├── README.md
    ├── charts
    │   ├── adminer-0.1.0.tgz
    │   ├── postgresql-7.1.0.tgz
    │   └── titanic-backend-1.0.0.tgz
    ├── requirements.lock
    ├── requirements.yaml
    ├── templates
    │   └── secret.yaml
    ├── titanic-backend
    │   ├── Chart.yaml
    │   ├── charts
    │   ├── requirements.yaml
    │   ├── templates
    │   │   ├── titanic-api-configmap.yaml
    │   │   ├── titanic-api-deployment.yaml
    │   │   ├── titanic-api-service.yaml
    │   │   └── titanic-swagger-ui-service.yaml
    │   └── values.yaml
    └── values.yaml

# Steps to use helm to deploy to minikube kubernetes cluster
    
    ## Go to this location and run to reflect any changes in charts this will create/update the packages in chart/
        `helm dep update`
    ##  Deploy app to your Kubernets Cluster
        
        Usage:
            helm install [CHART] [flags]
        
        `helm install . --name consol --namespace consol`
                or
        `helm upgrade --install consol . --namespace consol`

        This will create a helm release named consol and deploy our services inside namespace consol
