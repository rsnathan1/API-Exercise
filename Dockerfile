FROM python:3.7

RUN adduser --disabled-login titanic
WORKDIR /home/titanic
ENV PATH="/home/worker/.local/bin:${PATH}"

COPY requirements.txt .
RUN chown -R titanic:titanic requirements.txt
RUN pip install -r requirements.txt
COPY . .
RUN chown -R titanic:titanic .

EXPOSE 5000
# Run as non-root user
USER titanic
ENTRYPOINT [ "python" ]
CMD [ "server.py" ]
