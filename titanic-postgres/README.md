# Repo for creating Titanic Postgres DB
        .
    ├── Dockerfile
    ├── README.md
    ├── init-user-db.sh
    └── titanic.csv

    Dockerfile uses postgresql:11.5.0 from bitnami
    init-user-db.sh is a shell script which is copied to /docker-entrypoint-initdb.d/ inside postgres image which is picked up when container starts and db is initilaized with this data (titanic.csv).

# Steps to build and the repo to push the image
    `docker build . -t rahulswaminathan066/titanic-postgres:11.5`
    `docker push `rahulswaminathan066/titanic-postgres:11.5`