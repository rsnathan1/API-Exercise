### 1. Setup & fill database (Optional as we are already using a prebuilt image only needed if any changes in titanic.csv and schema)

In this project you'll find a csv-file [titanic.csv](titanic-postgres/titanic.csv)  with passenger data from the Titanic. 
Follow these steps to fill the database [titanic-postgres](titanic-postgres/).

### 2. Create an API

An HTTP-API that allows reading & writing,updating & deleting data from our database.
Tech stack used is Python Flask with Postgres . The API is described in [API.md](./API.md).
An OpenAPI specification is also provided (see [swagger.yml](./swagger.yml)). 

Relevant files for the same

    ├── API.md
    ├── Dockerfile  #Dockerfile for the API
    ├── README.md
    ├── __init__.py
    ├── config.py
    ├── models.py
    ├── people.py
    ├── person.py
    ├── requirements.txt #Python requirements
    ├── server.py
    ├── stack.yaml
    ├── swagger.yml
    ├── titanic-charts  #kubernetes helm configuration
    │   ├── Chart.yaml
    │   ├── README.md
    │   ├── charts
    │   │   ├── adminer-0.1.0.tgz
    │   │   ├── postgresql-7.1.0.tgz
    │   │   └── titanic-backend-1.0.0.tgz
    │   ├── requirements.lock
    │   ├── requirements.yaml
    │   ├── templates
    │   │   └── secret.yaml
    │   ├── titanic-backend
    │   │   ├── Chart.yaml
    │   │   ├── charts
    │   │   ├── requirements.yaml
    │   │   ├── templates
    │   │   │   ├── titanic-api-configmap.yaml
    │   │   │   ├── titanic-api-deployment.yaml
    │   │   │   ├── titanic-api-service.yaml
    │   │   │   └── titanic-swagger-ui-service.yaml
    │   │   └── values.yaml
    │   └── values.yaml
    └── titanic-postgres # postgres with data populated from titanic.csv
        ├── Dockerfile
        ├── README.md
        ├── init-user-db.sh
        └── titanic.csv

### 3. Dockerize (Optional as image for the the api has been prebuilt and pushed to docker registry)
    
    `docker build . -t rahulswaminathan066/titanic-backend:latest`
    `docker push `rahulswaminathan066/titanic-backend:latest`

#### Hints

- [Docker Install](https://www.docker.com/get-started)

### 4. Deploy to Kubernetes [MiniKube](https://kubernetes.io/docs/setup/minikube/) (free, local) Using Helm (Required)

1.Install [MiniKube](https://kubernetes.io/docs/setup/minikube/)

2.Enable ingres controller in Minikube
    `minikube addons enable ingress`
        
3.Follow these steps to deploy to kubernetes cluster [titanic-helm-charts](titanic-charts/)

4.Visit the service via NodePort:
    `minikube service titanic-api-service --url -n consol`   #service name of api service
    `minikube service titanic-swagger-service --url -n consol` #service name of aswagger service
    for eg. Output:
        `http://172.17.0.15:31637/ui`

### 5.Future improvements

Use Jenkins as CI and deploy to a cloud solution kubernetes or a container service
Link to my github repo for an example of CI using Jenkins and deploy to Azure [Rahul Swaminathan](https://github.com/rsnathan/azurecicd)